1) Name of all Section:
=====================================
--> Header Area
--> About Area
--> Skills Area
--> Portfolio Area
--> Achievement Area
--> Testimonials Area
--> Get In Touch
--> Footer

2) Template Size:
=====================================
--> Container: 1140px

3) Color:
=====================================
-->Theme-color/Primary-color: #526cbb
--> Secondary Color/ dark-color: #222222
--> Overlay/Gradient-color: background: linear-gradient(to right, #6b4da8 0%, #4a76c1 100%);


4) Fonts Collection:
=====================================
@import url('https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&family=Open+Sans:wght@300;400;600;700;800&display=swap');
font-family: 'Lato', sans-serif;
font-family: 'Open Sans', sans-serif;

5) Image Collection: 
=====================================
Check description box for all the images.

		   
