$(document).ready(function(){
	/*Typed.Js*/
		var typed = new Typed('.type', {
		  strings: [
		  'Designer',
		  'Developers' 
		  ],
		  typeSpeed: 60,
		  backSpeed: 60,
		  loop: true
		});

	/*easyPieChart*/
	$(".chart").easyPieChart({
		barColor: '#fff',
		trackColor: '#353535',
		size: 140,
		lineWidth: 8,
		lineCap: 'square',
		animate: 1000,
		scaleColor: false,
	});

	/*Mixitup*/
	var mixer = mixitup('.portfolio__items');

	/*Counter-Up*/
	$('.counter').counterUp({
		delay: 10,
		time: 1000
	});
});
